package az.ingress.databaseOperations;


import az.ingress.databaseOperations.model.Student1;
import az.ingress.databaseOperations.model.Student1$;
import az.ingress.databaseOperations.repository.Student1Repository;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
@Transactional
public class DatabaseOperationsApplication implements CommandLineRunner {

    private final Student1Repository studentRepository;
	private final EntityManagerFactory entityManagerFactory;

	public static void main(String[] args) {
		SpringApplication.run(DatabaseOperationsApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		studentRepository.getStudentsByJPQl("ov","Baki").stream().forEach(System.out::println);
        studentRepository.getStudentsBySqlQuery("ov","Baki").stream().forEach(System.out::println);

		JPAStreamer jpaStreamer = JPAStreamer.of(entityManagerFactory);
		jpaStreamer.stream(Student1.class)
				.filter(Student1$.surname.endsWith("ov"))
				.filter(Student1$.address.contains("Baki"))
				.forEach(System.out::println);


	}

}
