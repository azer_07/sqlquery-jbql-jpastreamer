package az.ingress.databaseOperations.dto;

import lombok.Data;

@Data
public class Student1Dto {

    private Long id;
    private String name;
    private String surname;
    private String address;
}
