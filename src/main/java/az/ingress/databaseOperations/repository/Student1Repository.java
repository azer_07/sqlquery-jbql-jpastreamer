package az.ingress.databaseOperations.repository;

import az.ingress.databaseOperations.model.Student1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface Student1Repository extends JpaRepository <Student1, Long> {
    @Query(value = "select * from student1 where student1.surname like %:surname and student1.address like %:address", nativeQuery = true )
   List<Student1> getStudentsBySqlQuery(String surname, String address);


    @Query(value = "select s from Student1 s  where s.surname like %:surname and s.address =:address")
    List<Student1> getStudentsByJPQl(String surname,String address);

}
